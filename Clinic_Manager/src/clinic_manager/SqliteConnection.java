//creted at 22/4/2017
//created by  Mahmoud AbdUlrasheed & Amir Saber & Doha Youssif

package clinic_manager;

/**
 *
 * @author Mahmoud AbdUlrasheed
 */
import java.sql.*;
public class SqliteConnection {
  public static Connection Connector() {
 try {
  Class.forName("org.sqlite.JDBC");
  Connection conn =DriverManager.getConnection("jdbc:sqlite:Clinic_Manager_DB.sqlite");
  return conn;
 } catch (ClassNotFoundException | SQLException e) {
  System.out.println(e);
  return null;
  // TODO: handle exception
 }
}
}