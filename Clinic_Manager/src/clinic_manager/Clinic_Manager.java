/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
// finished in day 20/4/2017
package clinic_manager;

import java.sql.*;
import javafx.application.Application;
import static javafx.application.Application.STYLESHEET_CASPIAN;
import static javafx.application.Application.STYLESHEET_MODENA;
import javafx.event.ActionEvent;
import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.control.TitledPane;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.stage.Stage;

/**
 *
 * @author Mahmoud AbdUlrasheed
 */
public class Clinic_Manager extends Application {
//temporary username and password
    String username = "clinic manager";
    String password = "1234";

    @Override
    public void start(Stage primaryStage) throws SQLException {
 //this for Login window
        GridPane pane = new GridPane();
        TitledPane pane1 = new TitledPane("Login", pane);
        VBox vb1 = new VBox();
        vb1.setAlignment(Pos.CENTER);
        vb1.setSpacing(50);
      
        vb1.setStyle("-fx-background-color: linear-gradient(to bottom, #D2D288, #F2F299);"
                + " -fx-border: 30px solid; -fx-border-color: white; -fx-background-radius: 5.0;"
                + " -fx-border-radius: 5.0");
        pane.setStyle("-fx-background-color: linear-gradient(to bottom, #D2D2D2, #F2F2a2);"
                + " -fx-border: 30px solid; -fx-border-color: white; -fx-background-radius: 15.0;"
                + " -fx-border-radius: 15.0");
        pane1.setStyle("-fx-background-color: linear-gradient(to bottom, #D2D2D2, #F2F2a2);"
                + " -fx-border: 30px solid; -fx-border-color: white; -fx-background-radius: 15.0;"
                + " -fx-border-radius: 15.0");
        Label l1 = new Label("Username ");
        Label l2 = new Label("Password ");
        Label l3 = new Label("    Clinic Manager    ");
     
        //instant for class connectionstate to be able to use label ->lcon
        CheckConnection CS =new CheckConnection(); 
        vb1.getChildren().addAll(
                l3, 
                CS.lcon, //the pane contains lable which defines the state of connection
                pane1
        );

        pane1.setMaxSize(400, 400);
        l1.setStyle(STYLESHEET_MODENA);
        l2.setStyle(STYLESHEET_MODENA);
        l1.setFont(Font.font(STYLESHEET_CASPIAN, FontWeight.BOLD, 18));
        l2.setFont(Font.font(STYLESHEET_CASPIAN, FontWeight.BOLD, 18));
        l3.setStyle("-fx-background-color: linear-gradient(to bottom, #FFD2D2, #F2CCCD);"
                + " -fx-border: 30px solid; -fx-border-color: white; -fx-background-radius: 45.0;"
                + " -fx-border-radius: 45.0");
        l3.setFont(Font.font(STYLESHEET_CASPIAN, FontWeight.BOLD, 30));
        TextField t1 = new TextField();
        PasswordField t2 = new PasswordField();

        t1.setPromptText("Username.");
        t2.setPromptText("Password.");

        Button btn1 = new Button("Login");

        pane.setAlignment(Pos.CENTER);
        pane.setPadding(new Insets(15, 5, 5, 5));
        pane.setHgap(10);
        pane.setVgap(10);
        pane.add(l1, 0, 0);
        pane.add(t1, 1, 0);
        pane.add(l2, 0, 1);
        pane.add(t2, 1, 1);
        pane.add(btn1, 1, 2);
        GridPane.setHalignment(btn1, HPos.RIGHT);
///////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////
//This for the program's Main  Window
        VBox vb = new VBox();

        GridPane pane3 = new GridPane();
        Button btn2 = new Button("Search Patient");
        Button btn3 = new Button("Add New Patient");
        TextField t4 = new TextField();
        t4.setPromptText("Please Enter Your Patient Name:");

        btn2.setFont(Font.font(STYLESHEET_MODENA, FontWeight.BOLD, 20));
        btn3.setFont(Font.font(STYLESHEET_MODENA, FontWeight.BOLD, 20));
        btn2.setMinSize(80, 30);
        btn3.setMinSize(80, 30);
        t4.setMinSize(300, 30);

        vb.setAlignment(Pos.CENTER);
        vb.setSpacing(50);

        pane3.setAlignment(Pos.CENTER);
        pane3.setPadding(new Insets(15, 5, 5, 5));
        pane3.setHgap(10);
        pane3.setVgap(10);
        pane3.add(btn2, 0, 0);
        pane3.add(t4, 1, 0);
        pane3.add(btn3, 0, 9);
        btn2.setMinSize(200, 30);
        btn3.setMinSize(200, 30);
        GridPane.setHalignment(btn2, HPos.LEFT);
        GridPane.setHalignment(btn3, HPos.LEFT);
        pane3.setStyle("-fx-background-color: linear-gradient(to bottom, #D2D288, #D2F299);"
                + " -fx-border: 30px solid; -fx-border-color: white; -fx-background-radius: 30.0;"
                + " -fx-border-radius: 30.0");
        vb.setStyle("-fx-background-color: linear-gradient(to bottom, #D2D288, #D2F2DD);"
                + " -fx-border: 30px solid; -fx-border-color: white; -fx-background-radius: 5.0;"
                + " -fx-border-radius: 5.0");

        vb.getChildren().addAll(
                new ImageView("c.png"),
                pane3
        );

        ////////////////////////////////////////////////
        VBox vb0 = new VBox();
        vb0.setAlignment(Pos.CENTER);
 vb0.setStyle("-fx-background-color: linear-gradient(to bottom, #D2D288, #F2F299);"
                + " -fx-border: 30px solid; -fx-border-color: white; -fx-background-radius: 5.0;"
                + " -fx-border-radius: 5.0");
 vb0.getChildren().add( new ImageView("n2.png"));
////////////////////////////////////////////////
        Scene scene = new Scene(vb0, 800, 400);
        Scene scene1 = new Scene(vb1, 800, 400);
        Scene scene2 = new Scene(vb, 1000, 600);
        primaryStage.setTitle("Clinic Manager");
        primaryStage.setScene(scene);
        primaryStage.show();

        //Actions to move from login window to the main program window
        // @Mona El-Nagar Helped to Handel Events
       
        vb0.setOnMouseClicked(e->
        {
        primaryStage.setScene(scene1);
        } );
        /////////////////////////////
        btn1.setOnAction((ActionEvent event) -> {
            if (t1.getText().equalsIgnoreCase(username) && t2.getText().contentEquals(password)) {
                primaryStage.setScene(scene2);
            }
            else{
            t2.setText(null);
            }

        });

        vb1.setOnKeyPressed((KeyEvent ke) -> {
            if (ke.getCode().equals(KeyCode.ENTER) && t1.getText().equalsIgnoreCase(username) && t2.getText().contentEquals(password)) {
                primaryStage.setScene(scene2);
            }
                else{
            t2.setText(null);
            }
        });

    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }

}
